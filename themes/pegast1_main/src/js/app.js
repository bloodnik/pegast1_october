/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
require("./main");

window.Vue = require("vue");
window.Vue.config.productionTip = false
window.Vue.config.devtools = false


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component("tourvisor-module", () => import(
    /* webpackChunkName: "tourvisor-module" */
    './components/Tourvisor/Module.vue'
));
Vue.component("osago-module", () => import(
    /* webpackChunkName: "osago-module" */
    './components/ExternalServices/Osago.vue'
));
Vue.component("main-slider", () => import(
    /* webpackChunkName: "main-slider" */
    './components/MainSlider.vue'
));
Vue.component("contact-tabs", () => import(
    /* webpackChunkName: "contact-tabs" */
    './components/ContactTabs.vue'
));
Vue.component("boss-form", require("./components/Forms/ToBossForm").default);
Vue.component("pick-me-tour", require("./components/Forms/PickMeTourForm").default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'

Vue.use(Viewer, {
    defaultOptions: {
        movable: false,
        toolbar: {
            prev: 4,
            next: 4,
        },
    }
})

const app = new Vue({
    el: "#app",
});
