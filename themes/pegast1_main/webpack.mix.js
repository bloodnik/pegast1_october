let mix = require('laravel-mix');


mix.babelConfig({
    plugins: ['@babel/plugin-syntax-dynamic-import'] // important to install -D
});

// Override mix internal webpack output configuration
mix.config.webpackConfig.output = {
    chunkFilename: 'assets/js/[name].bundle.js',
    publicPath: '/themes/pegast1_main/',
};

mix.options({
        processCssUrls: false,
        extractVueStyles: false,
        autoprefixer: {
            enabled: true,
            options: {
                overrideBrowserslist: ['last 2 versions', '> 1%'],
                cascade: true,
                grid: true,
            }
        },
    })
    .setPublicPath('/')
    .sass('src/scss/app.scss', 'assets/css/styles.css')
    .sass('src/scss/promo/main.scss', 'assets/css/promo.css')
    .js('src/js/app.js', 'assets/js/scripts.js')
    .js('src/js/promo.js', 'assets/js/promo.js')
    .js('src/js/tourvisior_init.js', 'assets/js/tourvisior_init.js')
    .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'assets/webfonts')
    .version();
