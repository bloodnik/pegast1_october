<?php

Route::post(
    'api/v1/sendFeedback',
    array(
        'uses' => 'Siril\Feedback\Controllers\FeedbackFormResults@send'
    )
);
