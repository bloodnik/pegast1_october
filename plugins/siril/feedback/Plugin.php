<?php namespace Siril\Feedback;

use Backend;
use System\Classes\PluginBase;

/**
 * Feedback Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Feedback',
            'description' => 'Форма обратной связи',
            'author'      => 'Siril',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }


    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'siril.feedback.some_permission' => [
                'tab' => 'Feedback',
                'label' => 'Доступ к результатам формы обратной связи'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'feedback' => [
                'label'       => 'Форма обратной связи',
                'url'         => Backend::url('siril/feedback/feedbackformresults'),
                'icon'        => 'icon-leaf',
                'permissions' => ['siril.feedback.*'],
                'order'       => 500,
            ],
        ];
    }
}
