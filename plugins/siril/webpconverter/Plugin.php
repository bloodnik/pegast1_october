<?php namespace Siril\Webpconverter;

use Backend;
use System\Classes\PluginBase;
use Imagick;
use Event;
use Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use System\Classes\MediaLibrary;

/**
 * webpconverter Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'WebP Converter',
            'description' => 'Plagin for convert uloaded images to WebP format',
            'author'      => 'siril',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('media.file.upload', function($mediaWidget, &$path, $uploadedFile) {
            $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
            $extension = strtolower($uploadedFile->getClientOriginalExtension());

            if (in_array($extension, $allowedExtensions)) {
                $srcPath = $uploadedFile->getPathname();
                $dstPath = $srcPath . '.webp';

                $imagick = new Imagick($srcPath);
                $imagick->setImageFormat('webp');
                $imagick->setImageCompressionQuality(95);

                $imagick->writeImage($dstPath);
                $imagick->destroy();

                $newPath = Request::input('path');
                $newPath = MediaLibrary::validatePath($newPath);
                $filePath = str_replace('.' . $extension, '.webp', $newPath . '/' . $uploadedFile->getClientOriginalName());

                MediaLibrary::instance()->put(
                    $filePath,
                    File::get($dstPath)
                );

                MediaLibrary::instance()->deleteFiles([$path]);

                $path = $filePath;
            }
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Siril\Webpconverter\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'siril.webpconverter.some_permission' => [
                'tab' => 'webpconverter',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'webpconverter' => [
                'label'       => 'webpconverter',
                'url'         => Backend::url('siril/webpconverter/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['siril.webpconverter.*'],
                'order'       => 500,
            ],
        ];
    }
}
