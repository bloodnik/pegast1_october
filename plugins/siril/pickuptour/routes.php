<?php

Route::post(
    'api/v1/sendPickUpForm',
    array(
        'uses' => 'Siril\PickUpTour\Controllers\PickUpFormResults@sendResult'
    )
);
