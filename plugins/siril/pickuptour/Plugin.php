<?php namespace Siril\PickUpTour;

use Backend;
use System\Classes\PluginBase;

/**
 * PickUpTour Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PickUpTour',
            'description' => 'No description provided yet...',
            'author'      => 'Siril',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Siril\PickUpTour\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'siril.pickuptour.access_records' => [
                'tab' => 'PickUpTour',
                'label' => 'Доступ к записям заявок на подбор'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'pickuptour' => [
                'label'       => 'Заявки на подбор',
                'url'         => Backend::url('siril/pickuptour/pickupformresults'),
                'icon'        => 'icon-leaf',
                'permissions' => ['siril.pickuptour.*'],
                'order'       => 500,
            ],
        ];
    }
}
