<?php namespace Siril\PickUpTour\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePickUpFormResultsTable extends Migration {
    public function up() {
        Schema::create('siril_pickuptour_pick_up_form_results', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('country');
            $table->integer('adults')->nullable();
            $table->integer('childs')->nullable();
            $table->integer('nights')->nullable();
            $table->date('date_from')->nullable();
            $table->integer('agreement');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('siril_pickuptour_pick_up_form_results');
    }
}
