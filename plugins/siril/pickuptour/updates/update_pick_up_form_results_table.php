<?php namespace Siril\PickUpTour\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePickUpFormResultsTable extends Migration {
    public function up() {
        Schema::table('siril_pickuptour_pick_up_form_results', function(Blueprint $table) {
            $table->string('office_city')->nullable();
        });
    }

    public function down() {
    }
}
