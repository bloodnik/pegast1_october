<?php



$result = array("hasError" => true, "msg" => "Ошибка");

if( (isset($_GET["name"]) && strlen($_GET["name"]) === 0) || (isset($_GET["phone"]) && strlen($_GET["phone"]) === 0 ) ) {
	$result["msg"] = "Не заполнены обязательные поля";
	echo json_encode($result);
	die();
}

$mailBody = "<div style=\"color: #000\">";
$mailBody .= "<p>Имя: " . $_GET["name"] . "</p>";
$mailBody .= "<p>Телефон: " . $_GET["phone"] . "</p>";
$mailBody .= "<p>Страна вылета: " . $_GET["country"] . "</p>";

$date = new DateTime($_GET["date"]);
$mailBody .= "<p>Дата вылета: " . $date->format('d.m.Y') . "</p>";
$mailBody .= "<p>Ночей: " . $_GET["nights"] . "</p>";
$mailBody .= "<p>Взрослых: " . $_GET["adults"] . "</p>";
$mailBody .= "<p>Детей: " . $_GET["childs"] . "</p>";
// $mailBody .= "<p>Звезд: " . implode(", ", $_GET["stars"]) . "</p>";
// $mailBody .= "<p>Питание: " . implode(", ", $_GET["meal"]) . "</p>";
$mailBody .= "</div>";


//$to  = "<i_siraev@mail.ru>, " ;
$to = "<pegastmgp@yandex.ru>";

$subject = "Заявка с сайта";

$message = ' <p>Текст письма</p> </br> <b>1-ая строчка </b> </br><i>2-ая строчка </i> </br>';

$headers  = "Content-type: text/html; charset=windows-utf-8 \r\n";
$headers .= "From: Pegas Touristic <noreply@pegast1.ru>\r\n";
//$headers .= "Reply-To: reply-to@example.com\r\n";

if(mail($to, $subject, $mailBody, $headers)) {
	$result["hasError"] = false;
	$result["msg"] = "Заявка успешно отправлена";
}


$api_key = 'vLsZl8Rr7wtH34E2F35MBur9enoum6F8mZTd1X9Yy5MNO79ZW74IbX5Tt5G70Zdm';
$url = 'https://golden-tours.moidokumenti.ru/api/add-lead';

$params = array(
	'name' => htmlspecialchars($_GET["name"]),
	'phone' => htmlspecialchars($_GET["phone"]),
	'source' => 'Заявка с сайта pegast1.ru',
	'fields' => array(
	array(
	'name' => 'Желаемая страна отдыха',
	'values' => array($_GET["country"])
	),
	array(
	'name' => 'Дата вылета',
	'values' => array( $date->format('d.m.Y'))
	),
	array(
	'name' => 'Ночей',
	'values' => array($_GET["nights"])
	),
	array(
	'name' => 'Взрослых',
	'values' => array($_GET["adults"])
	),
	array(
	'name' => 'Детей',
	'values' => array($_GET["childs"])
	)
));

$request = array(
'params' => json_encode($params),
'key' => $api_key
);

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 600);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
$result1 = curl_exec($ch);
curl_close($ch);


echo json_encode($result);



?>