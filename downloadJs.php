﻿<?php
// Список сторонних JS файлов для загрузки
$js_files = array(
    'https://mc.yandex.ru/metrika/tag.js' => 'metrika.js',
    'https://www.googletagmanager.com/gtag/js?id=G-2BEQ4BLNXC' => 'google_analitics.js',
    'https://vk.com/js/api/openapi.js?169' => 'vk_openapi.js',
);

// Папка на сервере, куда будут сохраняться загруженные файлы
$folder = 'js';

// Проходим по каждому файлу и загружаем его на сервер
foreach ($js_files as $js_file_url => $js_file_name) {
    // Определяем путь для сохранения файла на сервере
    $save_path = $folder . '/' . $js_file_name;

    // Загружаем файл на сервер
    file_put_contents($save_path, file_get_contents($js_file_url));
}

// Выводим сообщение об успешной загрузке файлов
echo 'JS файлы успешно загружены на сервер';
?>